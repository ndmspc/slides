#!/bin/bash

for f in $(ls *.mmd);do
  echo "Converting '$f' to '${f//.mmd/.svg}'"
  npx -p @mermaid-js/mermaid-cli mmdc -i $f -o ${f//.mmd/.svg} -c config.json --pdfFit
done
