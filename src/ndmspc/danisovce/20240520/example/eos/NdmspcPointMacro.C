#include <TCanvas.h>
#include <TF1.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>
#include <TH2.h>
#include <THnSparse.h>
#include <TList.h>
#include <TROOT.h>
#include <TString.h>
// #include <TStyle.h>
// #include "NdmspcUtils.h"

#include <nlohmann/json.hpp>
using json = nlohmann::json;

void NdmspcDefaultConfig(json & cfg)
{
  cfg = R"({
  "user": {
    "proj": [2,3],
    "verbose": 0
  },
  "ndmspc": {
    "data": {
      "file": "root://eos.ndmspc.io//eos/ndmspc/scratch/eos/workshop/NdmspcEos.root",
      "objects": ["hNdmspcEos"]
    },
    "cuts": [{"enabled": false,"axis": "sw", "rebin": 1,"bin": {"min": 1 ,"max": 1}}],
    "result": {
      "names": ["p0"]
    },
    "output": {
      "host": "",
      "dir": "",
      "file": "out.root",
      "opt": "?remote=1"
    },
    "process": {
      "type": "single"
    },
    "log": {
      "type": "always",
      "dir": "root://eos.ndmspc.io//eos/ndmspc/scratch/ndmspc/logs"
    },
    "job":{
      "inputs": [
        "dir|xrdcp|root://eos.ndmspc.io//eos/ndmspc/scratch/eos/workshop/sw/"
      ]
    },
    "verbose": 0
  }
})"_json;
}

TList * NdmspcProcess(TList * inputList, json cfg, THnSparse * finalResults, Int_t * point)
{

  int verbose = 0;
  if (!cfg["user"]["verbose"].is_null() && cfg["user"]["verbose"].is_number_integer()) {
    verbose = cfg["user"]["verbose"].get<int>();
  }

  if (inputList == nullptr) return nullptr;
  TList *  outputList = new TList();
  
  std::vector<int> projId = cfg["user"]["proj"];
  TString titlePostfix = "(no cuts)";
  if (cfg["ndmspc"]["projection"]["title"].is_string())
    titlePostfix = cfg["ndmspc"]["projection"]["title"].get<std::string>();
  THnSparse * hs = (THnSparse *)inputList->At(0);
  if (projId.size() == 1) {
    TH1 *h  = hs->Projection(projId[0], "O");
    h->SetNameTitle("hProj", TString::Format("hProj %s", titlePostfix.Data()).Data());
    h->SetStats(0);
    if (projId[0] == 0) {
      h->GetXaxis()->SetBinLabel(1,"salsa");
    } else   if (projId[0] == 1) {
      h->GetXaxis()->SetBinLabel(1,"ROOT");
    } else   if (projId[0] == 2) {
      h->GetXaxis()->SetBinLabel(1,"jsroot");
      h->GetXaxis()->SetBinLabel(2,"ndmvr");
    } else   if (projId[0] == 3) {
      h->GetXaxis()->SetBinLabel(1,"xrootd");
      h->GetXaxis()->SetBinLabel(2,"EOS");
    }
    h->SetMinimum(0);
    h->SetMaximum(100);
    outputList->Add(h);

    if (!gROOT->IsBatch()) {
      h->Draw();
    }
  } else if (projId.size() == 2) {
    TH2 *h2 = hs->Projection(projId[1], projId[0], "O");
    h2->SetNameTitle("hProj", TString::Format("hProj %s", titlePostfix.Data()).Data());
    h2->SetStats(0);
    if (projId[0] == 0) {
      h2->GetXaxis()->SetBinLabel(1,"salsa");
    } else   if (projId[0] == 1) {
      h2->GetXaxis()->SetBinLabel(1,"ROOT");
    } else   if (projId[0] == 2) {
      h2->GetXaxis()->SetBinLabel(1,"jsroot");
      h2->GetXaxis()->SetBinLabel(2,"ndmvr");
    } else   if (projId[0] == 3) {
      h2->GetXaxis()->SetBinLabel(1,"xrootd");
      h2->GetXaxis()->SetBinLabel(2,"EOS");
    }

    if (projId[1] == 1) {
      h2->GetYaxis()->SetBinLabel(1,"salsa");
    } else   if (projId[1] == 1) {
      h2->GetYaxis()->SetBinLabel(1,"ROOT");
    } else   if (projId[1] == 2) {
      h2->GetYaxis()->SetBinLabel(1,"jsroot");
      h2->GetYaxis()->SetBinLabel(2,"ndmvr");
    } else   if (projId[1] == 3) {
      h2->GetYaxis()->SetBinLabel(1,"xrootd");
      h2->GetYaxis()->SetBinLabel(2,"EOS");
    }

    h2->SetMinimum(0);
    h2->SetMaximum(100);
    outputList->Add(h2);

    if (!gROOT->IsBatch()) {
      h2->Draw();
    }

  }


  // Long64_t binid;
  // if (finalResults) {
  //   outputList->Add(finalResults);
  //   for (auto & name : cfg["ndmspc"]["result"]["names"]) {
  //     std::string n = name.get<std::string>();
  //     if (!n.compare("p0")) {
  //       point[0] = 0;
  //       binid    = finalResults->GetBin(point);
  //       finalResults->SetBinContent(point, h->Integral());
  //       finalResults->SetBinError(binid, TMath::Sqrt(h->Integral()));
  //     }
  //   }
  // }




  return outputList;
}
