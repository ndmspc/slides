#include <TAxis.h>
#include <TFile.h>
#include <THnSparse.h>
void NdmspcEos(std::string filename = "root://eos.ndmspc.io//eos/ndmspc/scratch/eos/workshop/NdmspcEos.root")
{
  TFile *fOut = TFile::Open(filename.c_str(), "RECREATE");

  const int dim = 4;
  int nbins = 1;
  Int_t bins[dim] = {nbins, nbins, nbins + 1, nbins + 1};
  Double_t xmin[dim] = {0., 0., 0., 0.};
  Double_t xmax[dim] = {(double)nbins, (double)nbins, (double)nbins + 1.0, (double)nbins + 1.0};
  THnSparse *hs = new THnSparseD("hNdmspcEos", "NDMSPC EOS", dim, bins, xmin, xmax);

  hs->GetAxis(0)->SetNameTitle("batch", "Batch system");
  hs->GetAxis(0)->SetBinLabel(1, "salsa");
  hs->GetAxis(1)->SetNameTitle("sw", "Analysis software tools");
  hs->GetAxis(1)->SetBinLabel(1, "ROOT");
  hs->GetAxis(2)->SetNameTitle("vis", "Visualization tools");
  hs->GetAxis(2)->SetBinLabel(1, "jsroot");
  hs->GetAxis(2)->SetBinLabel(2, "ndmvr");
  hs->GetAxis(3)->SetNameTitle("storage", "Storage technologies");
  hs->GetAxis(3)->SetBinLabel(1, "xrootd");
  hs->GetAxis(3)->SetBinLabel(2, "EOS");
  int point[dim] = {1, 1, 1, 1};
  point[2] = 1;
  point[3] = 1;
  hs->SetBinContent(point, 20);
  point[2] = 1;
  point[3] = 2;
  hs->SetBinContent(point, 22);
  point[2] = 2;
  point[3] = 1;
  hs->SetBinContent(point, 20);
  point[2] = 2;
  point[3] = 2;
  hs->SetBinContent(point, 22);

  hs->Write();

  fOut->Close();
  Printf("File stored at '%s' ...", filename.c_str());
}