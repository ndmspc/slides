#!/bin/bash
REVEALJS_VERSION=${REVEALJS_VERSION-"5.0.5"}
REVEALJS_MENU_PLUGIN_VERSION=${REVEALJS_MENU_PLUGIN_VERSION-"2.1.0"}
REVEALJS_PLUGINS_VERSION=${REVEALJS_PLUGINS_VERSION-"4.2.5"}
REVEALJS_NAME="reveal.js"
REVEALJS_MENU_PLUGIN_NAME="reveal.js-menu"
REVEALJS_PLUGINS_NAME="reveal.js-plugins"
REVEALJS="$REVEALJS_NAME-$REVEALJS_VERSION"
REVEALJS_MENU_PLUGIN="$REVEALJS_MENU_PLUGIN_NAME-$REVEALJS_MENU_PLUGIN_VERSION"
REVEALJS_PLUGINS="$REVEALJS_PLUGINS_NAME-$REVEALJS_PLUGINS_VERSION"
PRESENTATION_DIRS="slides"
JSON_CONFIG="presentations.json"

function LinkRevealJS() {
  local revealjs=$1
  local indexdir=$2
  rp=$(realpath --relative-to=$indexdir $revealjs)
  for l in css js dist plugin;do
    ln -sfn $rp/$l
  done
}

function GenerateRedirect() {
cat <<EOF > $2/index.html
<!DOCTYPE html>
<html>
<head>
  <title>Slides redirect</title>
  <meta http-equiv="refresh" content="0; URL=$1" />
</head>
<body>
  <p>Loading ...</p>
</body>
</html>
EOF

}

rm -rf public
mkdir -p public/assets

# Skopírovanie obsahu src/assets do public/assets
cp -a src/assets/* public/assets/

cd public
curl -sL https://github.com/hakimel/reveal.js/archive/$REVEALJS_VERSION.tar.gz | tar xz || { echo "Install 'curl' first!!!"; exit 1; }
cd $REVEALJS
echo "Downloaging '$REVEALJS_MENU_PLUGIN' ..."
curl -sL https://github.com/denehyg/$REVEALJS_MENU_PLUGIN_NAME/archive/refs/tags/$REVEALJS_MENU_PLUGIN_VERSION.tar.gz | tar xz || { echo "Install 'curl' first!!!"; exit 1; }
mv $REVEALJS_MENU_PLUGIN plugin/menu
echo "Downloaging '$REVEALJS_PLUGINS' ..."
curl -sL https://github.com/rajgoel/reveal.js-plugins/archive/refs/tags/$REVEALJS_PLUGINS_VERSION.tar.gz | tar xz || { echo "Install 'curl' first!!!"; exit 1; }

for rpd in $(find reveal.js-plugins-$REVEALJS_PLUGINS_VERSION -maxdepth 1 -mindepth 1 -type d);do
        echo "Adding plugin '$(basename $rpd) ..."
        cp -a $rpd plugin/
done
rm -rf $REVEALJS_PLUGINS
cd -


PUB_DIR=$(pwd)
cd $PRESENTATION_DIRS 
# Process all index.html files

# Inicializácia JSON súboru s počiatočnou štruktúrou
echo "{" > $PUB_DIR/$JSON_CONFIG
echo "  \"content\": [" >> $PUB_DIR/$JSON_CONFIG


for f in $(find . -name "index.html"); do
  d=$(dirname $f)
  d=${d/.\//}
  mkdir -p $PUB_DIR/$d
  cp -rf $d/ $PUB_DIR/$(dirname $d)

  # Extract title from index.html
  title=$(xmllint --html --xpath "string(//title)" $f 2>/dev/null)

  # Remove newlines and extra spaces
  title=$(echo "$title" | tr -d '\n' | xargs)

  # Generate JSON entry
  if [[ -n "$title" ]]; then
    echo "  {" >> $PUB_DIR/$JSON_CONFIG
    echo "    \"title\": \"$title\"," >> $PUB_DIR/$JSON_CONFIG
    echo "    \"path\": \"$d/index.html\"" >> $PUB_DIR/$JSON_CONFIG
    echo "  }," >> $PUB_DIR/$JSON_CONFIG
  fi
done

cd -

# Dokončenie JSON konfigurácie (odstránenie poslednej čiarky a uzavretie poľa)
sed -i '' '$ s/,$//' $PUB_DIR/$JSON_CONFIG
echo "  ]" >> $PUB_DIR/$JSON_CONFIG
echo "}" >> $PUB_DIR/$JSON_CONFIG

for p in $(find $PUB_DIR -name index.html -not -path "*reveal.js*" );do
  echo $p
  pd=$(dirname $p)
  cd $pd
  LinkRevealJS $PUB_DIR/$REVEALJS .
  cd -
  # GenerateRedirect $(basename $pd) $(pwd)
done
cd -

# # yarn run build
# yarn run preview
