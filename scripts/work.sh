#!/bin/bash
REVEALJS_VERSION=${REVEALJS_VERSION-"5.0.5"}
REVEALJS_MENU_PLUGIN_VERSION=${REVEALJS_MENU_PLUGIN_VERSION-"2.1.0"}
REVEALJS_PLUGINS_VERSION=${REVEALJS_PLUGINS_VERSION-"4.2.5"}
REVEALJS_NAME="reveal.js"
REVEALJS_MENU_PLUGIN_NAME="reveal.js-menu"
REVEALJS_PLUGINS_NAME="reveal.js-plugins"
REVEALJS="$REVEALJS_NAME-$REVEALJS_VERSION"
REVEALJS_MENU_PLUGIN="$REVEALJS_MENU_PLUGIN_NAME-$REVEALJS_MENU_PLUGIN_VERSION"
REVEALJS_PLUGINS="$REVEALJS_PLUGINS_NAME-$REVEALJS_PLUGINS_VERSION"
PRESENTATION_DIRS="src"

function LinkRevealJS() {
  local revealjs=$1
  local indexdir=$2
  rp=$(realpath --relative-to=$indexdir $revealjs)
  for l in css js lib plugin;do
    ln -sfn $rp/$l
  done
}


if [ ! -d $REVEALJS ];then
        echo "Downloaging '$REVEALJS' ..."
        curl -sL https://github.com/hakimel/reveal.js/archive/$REVEALJS_VERSION.tar.gz | tar xz
  cd $REVEALJS

  echo "Downloaging '$REVEALJS_MENU_PLUGIN' ..."
  curl -sL https://github.com/denehyg/$REVEALJS_MENU_PLUGIN_NAME/archive/refs/tags/$REVEALJS_MENU_PLUGIN_VERSION.tar.gz | tar xz || { echo "Install 'curl' first!!!"; exit 1; }
  mv $REVEALJS_MENU_PLUGIN plugin/menu
  echo "Downloaging '$REVEALJS_PLUGINS' ..."
  curl -sL https://github.com/rajgoel/$REVEALJS_PLUGINS_NAME/archive/refs/tags/$REVEALJS_PLUGINS_VERSION.tar.gz | tar xz || { echo "Install 'curl' first!!!"; exit 1; }

  for rpd in $(find reveal.js-plugins-$REVEALJS_PLUGINS_VERSION -maxdepth 1 -mindepth 1 -type d);do
          echo "Adding plugin '$(basename $rpd) ..."
          cp -a $rpd plugin/
  done
  rm -rf $REVEALJS_PLUGINS
  npm install || { echo "Error running 'npm install' in $REVEALJS directory !!!"; exit 1; }
  cd -
fi


if [ -n "$1" ];then
	[ -d $PRESENTATION_DIRS/$1 ] || { echo "Directory '$PRESENTATION_DIRS/$1' doesn't exists !!!"; exit 2; }
	echo "Switching to $PRESENTATION_DIRS/$1"
	rm -f $REVEALJS/index.html
	for d in $(find $PRESENTATION_DIRS/$1 -mindepth 1 -maxdepth 1);do
		ln -sfn ../$d $REVEALJS/$(basename $d)
	done

  ln -sfn $REVEALJS work

	echo ""
	echo "You are now set for presentation '$PRESENTATION_DIRS/$1' in '$REVEALJS' which points to 'work' directory"
	echo ""


  if [ "$2" == "start" ];then
    cd $REVEALJS && npm start
  else
    echo "Now you have to do:"
    echo ""
    echo "  $0 $* start"
    echo ""
    echo "  or manualy"
    echo ""
    echo "  cd work"
    echo "  npm start"
    echo ""
    echo "Edit 'index.html' in 'work' directory"
    echo ""
  fi

else
	for p in $(find $PRESENTATION_DIRS -name index.html -not -path "*reveal.js*" );do
		p=${p//src\//}
		p=${p//\/index.html/}
		echo "  $p"
	done
fi
